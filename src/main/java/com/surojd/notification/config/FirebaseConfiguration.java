/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.surojd.notification.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 *
 * @author acer
 */
@Configuration
public class FirebaseConfiguration {

  public void FirebaseConfig() throws Exception {
    Resource file = new ClassPathResource("firebase/auth.json");
    FirebaseOptions options = new FirebaseOptions.Builder()
      .setCredentials(GoogleCredentials.fromStream(file.getInputStream()))
      .setDatabaseUrl("https://asal-pasal.firebaseio.com").build();
    FirebaseApp.initializeApp(options);
  }
}
