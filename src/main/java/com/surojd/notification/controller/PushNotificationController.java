/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.surojd.notification.controller;

import com.google.firebase.messaging.Notification;
import com.surojd.notification.util.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author acer
 */
@RestController
@RequestMapping("/push")
public class PushNotificationController {

  @Autowired
  NotificationService notificationService;

  public String sendPushNotification(@RequestParam("token") String firebaseToken) throws Exception {
    notificationService.pushNotification(firebaseToken, Notification.builder()
      .setTitle("title")
      .setBody("body")
      .build());

    return "Ok";
  }
}
