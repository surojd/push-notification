/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.surojd.notification.util;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import org.springframework.stereotype.Service;

/**
 *
 * @author acer
 */
@Service
public class NotificationService {

  public void pushNotification(String firebaseToken, Notification dto) throws Exception {
    // See documentation on defining a message payload.
    Message message = Message.builder()
      .setNotification(dto)
      .build();

// Send a message to devices subscribed to the combination of topics
// specified by the provided condition.
    String response = FirebaseMessaging.getInstance().send(message);
// Response is a message ID string.
    System.out.println("Successfully sent message: " + response);
  }
}
